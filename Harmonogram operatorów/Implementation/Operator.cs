﻿using Harmonogram_operatorów.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów.Implementation
{
    public class Operator : IOperator
    {
        public string Name
        {
            get;
            private set;
        }
        public IList<DateTime> Leave
        {
            get;
            private set;
        }

        public Operator() { }
        public Operator(string name, IList<DateTime> leave)
        {
            this.Name = name;
            this.Leave = leave;
        }

        public void AddLeaveDay(DateTime day)
        {
            if (!Leave.Contains(day))
            {
                Leave.Add(day);
            }
        }
        public void RemoveLeaveDay(DateTime day)
        {
            if (Leave.Contains(day))
            {
                Leave.Remove(day);
            }
        }

        public bool IsAvailable(DateTime day)
        {
            return !Leave.Contains(day);
        }
    }
}
