﻿using Harmonogram_operatorów.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów.Implementation
{
    public class Harmonogram : IHarmonogram
    {
        protected IDictionary<DateTime, Operator> PastOrderedSchedule;
        protected IDictionary<DateTime, Operator> CurrentOrderedSchedule;
        protected IList<DateTime> Holidays;
        protected IList<Operator> Operators;
        private Operator NotAvailableOperator = new Operator("-", null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pastSchedule">Dictionary with days and assigned to them operators</param>
        /// <param name="futureHolidays">List with incoming holidays</param>
        public Harmonogram(IDictionary<DateTime, Operator> pastSchedule, IList<DateTime> futureHolidays)
        {
            this.PastOrderedSchedule = new SortedDictionary<DateTime, Operator>();
            this.CurrentOrderedSchedule = new SortedDictionary<DateTime, Operator>();
            this.Holidays = new List<DateTime>();
            this.Operators = new List<Operator>();

            SeedPastSchedule(pastSchedule);
            SeedHolidays(futureHolidays);
            SeedOperators();
        }

        public void GenerujHarmonogram()
        {
            for (int i = 0; i < 10; i++)
            {
                FindNextDuty();
            }
        }

        public void WyświetlHarmonogram()
        {
            Console.WriteLine(this.ToString());
        }

        private void SeedOperators()
        {
            foreach (var item in this.PastOrderedSchedule)
            {
                if (!this.Operators.Contains(item.Value))
                {
                    this.Operators.Add(item.Value);
                }
            }
        }

        private void SeedPastSchedule(IDictionary<DateTime, Operator> schedule)
        {
            foreach (var day in schedule)
            {
                if (!this.PastOrderedSchedule.Contains(day))
                {
                    this.PastOrderedSchedule.Add(day);
                }
            }
        }
        private void SeedHolidays(IList<DateTime> holidays)
        {
            foreach (var day in holidays)
            {
                if (!this.Holidays.Contains(day))
                {
                    this.Holidays.Add(day);
                }
            }

            SortHolidays();
        }
        private void SortHolidays()
        {
            this.Holidays.OrderBy(item => item);
        }
        private void FindNextDuty()
        {
            DateTime firstDayOfDuty = FindNextJobDay(LastScheduledDayOrDefault());
            DateTime secondDayOfDuty = FindNextJobDay(firstDayOfDuty);
            DateTime thirdDayOfDuty = FindNextJobDay(secondDayOfDuty);

            Operator chosenOperator = FindOperator(firstDayOfDuty, secondDayOfDuty, thirdDayOfDuty);

            this.CurrentOrderedSchedule.Add(firstDayOfDuty, chosenOperator);
            this.CurrentOrderedSchedule.Add(secondDayOfDuty, chosenOperator);
            this.CurrentOrderedSchedule.Add(thirdDayOfDuty, chosenOperator);
        }
        private DateTime FindNextJobDay(DateTime previousDay)
        {
            int nextDay = 1;
            DateTime currentDay = previousDay.AddDays(nextDay);
            while (true)
            {
                if (!IsHoliday(currentDay) && currentDay.DayOfWeek != DayOfWeek.Saturday && currentDay.DayOfWeek != DayOfWeek.Sunday)
                {
                    return currentDay;
                }
                currentDay = currentDay.AddDays(nextDay);

            }
        }
        private Operator FindOperator(DateTime firstDay, DateTime secondDay, DateTime thirdDay)
        {
            var operators = SortedOperatorsByLeastJobsForLastMonth();
            var lastOperator = GetLastOperatorOrDefault();

            if (lastOperator != null && lastOperator == operators[0])
            {
                operators.RemoveAt(0);
            }

            foreach (var item in operators)
            {
                if (CheckIfOperatorIsAvailable(item, firstDay, secondDay, thirdDay))
                {
                    return item;
                }
            }

            if (lastOperator != null && CheckIfOperatorIsAvailable(lastOperator, firstDay, secondDay, thirdDay))
            {
                return lastOperator;
            }

            return NotAvailableOperator;

        }
        private IList<Operator> SortedOperatorsByLeastJobsForLastMonth()
        {
            IDictionary<Operator, int> sortedOperatorsWithDaysWorked = new Dictionary<Operator, int>();
            foreach (var item in Operators)
            {
                sortedOperatorsWithDaysWorked.Add(item, 0);
            }

            IEnumerable<KeyValuePair<DateTime, Operator>> lastMonth = GetLastMonthOfSchedule();
            foreach (var item in lastMonth)
            {
                if (item.Value.Name.Equals("-"))
                {
                    continue;
                }
                sortedOperatorsWithDaysWorked[item.Value] += 1;
            }

            var sortedList = from entry in sortedOperatorsWithDaysWorked orderby entry.Value ascending select entry.Key;
            return sortedList.ToList();
        }
        private IEnumerable<KeyValuePair<DateTime, Operator>> GetLastMonthOfSchedule()
        {
            IDictionary<DateTime, Operator> returnDict = new Dictionary<DateTime, Operator>();
            var countOfCurrent = CurrentOrderedSchedule.Count;
            if (countOfCurrent < 30)
            {
                foreach (var item in CurrentOrderedSchedule.Take(countOfCurrent))
                {
                    returnDict.Add(item.Key, item.Value);
                }
                foreach (var item in PastOrderedSchedule.Reverse().Take(30 - countOfCurrent))
                {
                    returnDict.Add(item.Key, item.Value);
                }
                return returnDict;
            }

            return this.PastOrderedSchedule.Reverse().Take(30);
        }

        private DateTime LastScheduledDayOrDefault()
        {
            if (this.CurrentOrderedSchedule.Count == 0)
            {
                if (this.PastOrderedSchedule.Count == 0)
                    return DateTime.Now.Date;
                else
                    return this.PastOrderedSchedule.Last().Key.Date;
            }

            return this.CurrentOrderedSchedule.Keys.Last();
        }

        private bool IsHoliday(DateTime day)
        {
            foreach (var item in Holidays)
            {
                if (item.Date.CompareTo(day.Date) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckIfOperatorIsAvailable(Operator man, DateTime first, DateTime second, DateTime third)
        {
            return man.IsAvailable(first) && man.IsAvailable(second) && man.IsAvailable(third);
        }

        private Operator GetLastOperatorOrDefault()
        {
            if (CurrentOrderedSchedule.Count == 0)
            {
                return null;
            }

            return CurrentOrderedSchedule.Last().Value;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Data\t\tOperator\n");
            foreach (var item in CurrentOrderedSchedule)
            {
                sb.AppendLine(item.Key.ToShortDateString() + "\t" + item.Value.Name);
            }

            return sb.ToString();
        }

        public void GenerujZmienionyHarmonogram(Operator oper, DateTime newFreeDay)
        {
            oper.AddLeaveDay(newFreeDay);

            if (!CheckIfChangeNeeded(oper, newFreeDay))
            {
                return;
            }

            GenerateScheduleFromDate(newFreeDay);
            
        }

        private bool CheckIfChangeNeeded(Operator oper, DateTime newFreeDay)
        {
            Operator foundOperator;
            if (!this.CurrentOrderedSchedule.TryGetValue(newFreeDay, out foundOperator))
            {
                return false;
            }

            return foundOperator == oper;
            
        }

        private void GenerateScheduleFromDate(DateTime startDate)
        {
            ClearFutureAfterDate(startDate);

            GenerujHarmonogram();
        }

        private void ClearFutureAfterDate(DateTime date)
        {
            var datesToPastSchedule = this.CurrentOrderedSchedule.Where(t => DateTime.Compare(t.Key, date) < 0);
            foreach (var item in datesToPastSchedule)
            {
                this.PastOrderedSchedule.Add(item);
            }

            this.CurrentOrderedSchedule.Clear();
        }
    }
}
