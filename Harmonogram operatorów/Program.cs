﻿using Harmonogram_operatorów.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<Operator> operators = Seed.Seed.GetOperators();
            IDictionary<DateTime, Operator> pastSchedule = Seed.Seed.GetPastSchedule(operators);
            IList<DateTime> holidays = Seed.Seed.GetFutureHolidays();

            Harmonogram harm = new Harmonogram(pastSchedule, holidays);
            harm.GenerujHarmonogram();
            harm.WyświetlHarmonogram();

            Console.WriteLine();

            harm.GenerujZmienionyHarmonogram(operators[0], new DateTime(2015, 1, 22));
            harm.WyświetlHarmonogram();

            Console.WriteLine();

            harm.GenerujZmienionyHarmonogram(operators[2], new DateTime(2015, 2, 2));
            harm.WyświetlHarmonogram();

            Console.WriteLine();

            harm.GenerujZmienionyHarmonogram(operators[1], new DateTime(2015, 2, 10));
            harm.WyświetlHarmonogram();

            Console.ReadKey();
        }
    }
}
