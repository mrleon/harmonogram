﻿using Harmonogram_operatorów.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów.Contract
{
    public interface IHarmonogram
    {
        void GenerujHarmonogram();
        void WyświetlHarmonogram();
        void GenerujZmienionyHarmonogram(Operator oper, DateTime newFreeDay);

    }
}
