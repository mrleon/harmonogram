﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów.Contract
{
    public interface IOperator
    {
        string Name { get; }
        IList<DateTime> Leave { get; }
        void AddLeaveDay(DateTime day);
        void RemoveLeaveDay(DateTime day);
    }
}
