﻿using Harmonogram_operatorów.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonogram_operatorów.Seed
{
    public static class Seed
    {
        public static IList<Operator> GetOperators()
        {
            IList<Operator> operators = new List<Operator>();

            operators.Add(new Operator("Jarosław R", new List<DateTime>() { new DateTime(2015, 1, 2), new DateTime(2015, 1, 8), new DateTime(2015, 1, 20), new DateTime(2015, 1, 21) }));
            operators.Add(new Operator("Mirek", new List<DateTime>() { new DateTime(2015, 1, 2), new DateTime(2015, 1, 8), new DateTime(2015, 1, 11), new DateTime(2015, 1, 22) }));
            operators.Add(new Operator("Bogumiła Niewiadomska", new List<DateTime>() { new DateTime(2015, 1, 2), new DateTime(2015, 1, 8), new DateTime(2015, 1, 22), new DateTime(2015, 1, 23) }));
            operators.Add(new Operator("Filip", new List<DateTime>() { new DateTime(2015, 1, 2), new DateTime(2015, 1, 8), new DateTime(2015, 1, 9), new DateTime(2015, 1, 12), new DateTime(2015, 1, 13) }));
            operators.Add(new Operator("Ala", new List<DateTime>() { new DateTime(2015, 1, 19), new DateTime(2015, 1, 20), new DateTime(2015, 1, 21), new DateTime(2015, 1, 22), new DateTime(2015, 1, 23), new DateTime(2015, 1, 24), }));

            return operators;
        }

        public static IDictionary<DateTime, Operator> GetPastSchedule(IList<Operator> operators)
        {
            IDictionary<DateTime, Operator> days = new Dictionary<DateTime, Operator>();

            days.Add(new DateTime(2014, 12, 1), operators[0]);
            days.Add(new DateTime(2014, 12, 2), operators[1]);
            days.Add(new DateTime(2014, 12, 3), operators[1]);
            days.Add(new DateTime(2014, 12, 4), operators[1]);
            days.Add(new DateTime(2014, 12, 5), operators[2]);
            days.Add(new DateTime(2014, 12, 8), operators[2]);
            days.Add(new DateTime(2014, 12, 9), operators[2]);
            days.Add(new DateTime(2014, 12, 10), operators[3]);
            days.Add(new DateTime(2014, 12, 11), operators[3]);
            days.Add(new DateTime(2014, 12, 12), operators[3]);
            days.Add(new DateTime(2014, 12, 15), operators[4]);
            days.Add(new DateTime(2014, 12, 16), operators[4]);
            days.Add(new DateTime(2014, 12, 17), operators[4]);
            days.Add(new DateTime(2014, 12, 18), operators[2]);
            days.Add(new DateTime(2014, 12, 19), operators[2]);
            days.Add(new DateTime(2014, 12, 20), operators[2]);
            days.Add(new DateTime(2014, 12, 22), operators[3]);
            days.Add(new DateTime(2014, 12, 23), operators[3]);
            days.Add(new DateTime(2014, 12, 24), operators[3]);
            days.Add(new DateTime(2014, 12, 29), operators[0]);
            days.Add(new DateTime(2014, 12, 30), operators[0]);
            days.Add(new DateTime(2014, 12, 31), operators[0]);

            return days;
        }

        public static IList<DateTime> GetFutureHolidays()
        {
            IList<DateTime> holidays = new List<DateTime>();

            holidays.Add(new DateTime(2015, 1, 1));
            holidays.Add(new DateTime(2015, 1, 6));
            holidays.Add(new DateTime(2015, 1, 15));
            holidays.Add(new DateTime(2015, 1, 30));
            holidays.Add(new DateTime(2015, 2, 14));
            holidays.Add(new DateTime(2015, 2, 19));
            holidays.Add(new DateTime(2015, 2, 20));

            return holidays;
        }


    }
}
